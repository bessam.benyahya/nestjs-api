import { Test } from '@nestjs/testing';
import { CreateTodoDto } from 'src/todo/dto';
import { TodoService } from 'src/todo/todo.service';
import { AppModule } from '../../../app.module';
import { PrismaService } from '../../../prisma/prisma.service';

describe('TodoService Int', () => {
  let prisma: PrismaService;
  let todoService: TodoService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    prisma = moduleRef.get(PrismaService);
    await prisma.cleanDb();

    todoService = moduleRef.get(TodoService);
  });

  describe('createTodo()', () => {
    let userId: number;

    const dto: CreateTodoDto = {
      title: 'todo',
      description: 'description',
      status: 'OPEN',
    };

    it('should create user', async () => {
      const user = await prisma.user.create({
        data: {
          email: 'bessam@binov.com',
          hash: 'xxxx',
          firstName: 'Bessam',
          lastName: 'BenYahis',
        },
      });

      userId = user.id;
    });

    it('should create todo', async () => {
      const todo = await todoService.createTodo(userId, dto);

      expect(todo.title).toBe(dto.title);
      expect(todo.description).toBe(dto.description);
      expect(todo.status).toBe(dto.status);
      expect(todo.userId).toBe(userId);
    });

    it('should throw on duplicate title', async () => {
      const todo = await todoService
        .createTodo(userId, dto)
        .then((todo) => expect(todo).toBe(undefined))
        .catch((error) => {
          expect(error.status).toBe(403);
          expect(error.message).toBe('Duplicate todo title');
        });
    });
  });
});
