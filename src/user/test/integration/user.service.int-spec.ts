import { Test } from '@nestjs/testing';
import { CreateTodoDto } from 'src/todo/dto';
import { UserService } from 'src/user/user.service';
import { AppModule } from '../../../app.module';
import { PrismaService } from '../../../prisma/prisma.service';

describe('UserService Int', () => {
  let prisma: PrismaService;
  let userService: UserService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    prisma = moduleRef.get(PrismaService);
    await prisma.cleanDb();

    userService = moduleRef.get(UserService);
  });

  describe('createUser()', () => {
    const email = 'bessam@binov.com';
    const password = 'xxxx';

    it('should create user', async () => {
      const user = await userService.createUserDeprecated(email, password);

      expect(user.email).toBe(email);
      expect(user.hash).toBe(password);
    });

    it('should throw on duplicate email', async () => {
      try {
        const user = await userService.createUserDeprecated(email, password);
      } catch (error) {
        expect(error.status).toBe(403);
        expect(error.message).toBe('Duplicate email');
      }
    });
  });
});
